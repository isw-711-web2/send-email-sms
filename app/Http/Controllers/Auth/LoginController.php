<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Auth\Authenticatable;
use Nexmo;
use App\SendCode;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    // public function authenticated(Request $request, Authenticatable $user)
    // {
    //     Auth::logout();
    //     $request->session()->put('nexmo:user:id', $user->id);
    //     $verification = Nexmo::verify()->start([
    //         'number' => $user->phone,
    //         'brand' => '2-FA',
    //     ]);
    //     $request->session()->put('nexmo:request_id', $verification->getRequestId());
    //     return redirect('/factor');
    // }

    


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = RouteServiceProvider::HOME;
    //protected $redirectTo = '/verify'; //funciona
   
     //protected $redirectTo = '/login'; //prueba
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // public function login(Request $request){
    //     $this->validateLogin($request);
    //     if ($this->hasTooManyLoginAttempts($request)) {
    //         $this->fireLockoutResponse($request);
    //         return $this->sendLockoutResponse($request);
    //     }

    //     if ($this->guard()->validate($this->credentials($request))) {
    //         $user = $this->guard()->getLastAttempted();
    //         if ($user->active && $this->attemptLogin($request)) {
    //             return $this->sendLoginResponse($request);
    //         }
    //     }
    //     else {
    //         $this->incrementLoginAttempts($request);
    //         $user->code = SendCode::sendCode($user->phone);
    //         if ($user->save()) {
    //             return redirect('/verify?phone=' .$user->phone);
    //         }
    //     }
    //     $this->incrementLoginAttempts($request);
    //     return $this->sendFailedLoginResponse($request);
    // }

    protected function authenticated(Request $request, $user){
        // $user->sendCode();
        if($user){
          $user->code = SendCode::sendCode($user->phone);
            $user->save();
            
            return redirect('/verify?phone=' .$user->phone);
        }   
    }

  
}
